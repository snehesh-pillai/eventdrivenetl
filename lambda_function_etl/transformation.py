import pandas as pd
def transform(df_NYT, df_JH):
    df_NYT['date'] = pd.to_datetime((df_NYT['date']))
    df_JH['Date'] = pd.to_datetime((df_JH['Date']))
    merge_df = pd.merge(df_NYT,df_JH,how="inner",left_on="date",right_on="Date")
    is_US = merge_df["Country/Region"] == "US"
    merge_df = merge_df[is_US]
    merge_df = merge_df.drop(merge_df.columns[[0, 2, 4,5,6,7]], axis=1)
    return merge_df