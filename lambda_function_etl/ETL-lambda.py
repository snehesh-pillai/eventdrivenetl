import json
import pandas as pd
import transformation
import os
import boto3



__TableName__ = "covid_US"
primary_column = "date"
dynamodb = boto3.client('dynamodb')
db = boto3.resource("dynamodb")
table = db.Table(__TableName__)
ssm_client = boto3.client('ssm')

# def publishMessage(message):
#     sns_client = boto3.client('sns')
#     sns_client.publish(
#         TopicArn=os.environ['topic_name'],
#         Message=message,
#         Subject='US Covid Cases'
#     )
    
def put_parameter(index):
    response = ssm_client.put_parameter(
    Name='index_key',
    Description='last updated index',
    Value=index,
    Type='String',
    Overwrite=True)

def get_parameter():
    response = ssm_client.get_parameter(
    Name='index_key')
    return response['Parameter']['Value']
    

def first_insert(historical_data):
    counter = 0
    last_date = ""
    for index, item in historical_data.iterrows():
        row = {"Date":{'S':str(item["Date"])},'Cases':{'N':str(item['cases'])},"Confirmed":{'N':str(item["Confirmed"])},"Recovered":{'N':str(int(item["Recovered"]))},"Deaths":{'N':str(item["Deaths"])}}
        dynamodb.put_item(TableName='covid_US', Item=row)
        last_date = str(item["Date"])
        counter = counter + 1
    put_parameter(last_date)
    message = f"Total number of cases reported in US till date is \n {counter}"
    #publishMessage(message)
    
                
def every_day_insert(historical_data):
    counter = 0
    last_updated_date_ssm = get_parameter()
    #last_date_dynamo = table.query(KeyConditionExpression=Key('date').eq(last_updated_date_ssm))
    current_index = historical_data[historical_data['Date']==str(last_updated_date_ssm)].index.item()
    for index,item in historical_data.iterrows():
            if int(index) > int(current_index):
                row = {"Date":{'S':str(item["Date"])},'Cases':{'N':str(item['cases'])},"Confirmed":{'N':str(item["Confirmed"])},"Recovered":{'N':str(int(item["Recovered"]))},"Deaths":{'N':str(item["Deaths"])}}
                counter = counter + 1
                dynamodb.put_item(TableName='covid_US', Item=row)
                last_date = item["Date"]
                put_parameter(str(last_date))
    return counter

    
    
 
def lambda_handler(event, context):
    isFirst = False
    
    url_NYT=os.environ["url_NYT"]
    url_JH = os.environ["url_JH"]
    df_NYT = pd.read_csv(url_NYT, error_bad_lines=False)
    df_JH = pd.read_csv(url_JH, error_bad_lines=False)
    transformed_df = transformation.transform(df_NYT, df_JH)
    try:
        if get_parameter:
            counter = every_day_insert(transformed_df)
            message = f"Total number of records updated today \n {counter}"
            #publishMessage(message)
            
    except ssm_client.exceptions.ParameterNotFound:
        first_insert(transformed_df)
        
        
    
    
        
    
   
    
    
    
    
    
    
    
    
    
