resource "aws_iam_role_policy" "lambda_policy" {
  name = "lambda_policy"
  role = aws_iam_role.lambda_role.id

  policy = "${file("iam/lambda-policy.json")}"
}

resource "aws_iam_role" "lambda_role" {
  name = "lambda_role"
  assume_role_policy = "${file("iam/lambda-assume-role.json")}"
}


resource "aws_iam_role" "lambda_role_for_firehose" {
  name = "lambda_role_for_firehose"
  assume_role_policy = "${file("iam/lambda-assume-role-firehose.json")}"
}

resource "aws_iam_role_policy" "lambda_policy_for_firehose" {
  name = "lambda_policy_for_firehose"
  role = aws_iam_role.lambda_role_for_firehose.id

  policy = "${file("iam/lambda-policy-firehose.json")}"
}


resource "aws_iam_role" "role_for_firehose_access_S3" {
  name = "role_for_firehose_access_S3"
  assume_role_policy = "${file("iam/role_for_firehose_access_S3.json")}"
}

resource "aws_iam_role_policy" "policy-for-firehose-access_S3" {
  name = "policy_for_firehose_access_S3"
  role = aws_iam_role.role_for_firehose_access_S3.id

  policy = "${file("iam/policy-for-firehose-access-S3.json")}"
}
