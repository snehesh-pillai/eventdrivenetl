locals {
    lambda_zip_location = "/tmp/lambda_function_etl.zip"
}


module "admin-sns-email-topic" {
  source = "github.com/deanwilson/tf_sns_email"

  display_name  = "Covid Update"
  email_address = "admin@gmail.com"
  stack_name    = "admin-sns-email"
}



data "archive_file" "lambda_function_etl" {
  type        = "zip"
  output_path = local.lambda_zip_location
  source_dir = "lambda_function_etl"
}
resource "aws_lambda_function" "test_lambda" {
  filename      = local.lambda_zip_location
  function_name = "lambda_function_etl"
  role          = aws_iam_role.lambda_role.arn
  handler       = "ETL-lambda.lambda_handler"
  layers = ["arn:aws:lambda:us-west-2:251566558623:layer:python37-layer-pandas-gbq:1"]
  memory_size = "192"
  timeout = "20"

  #source_code_hash = filebase64sha256("tmp/lambda_function_etl.zip")
  runtime = "python3.7"

  environment {
    variables = {
      url_JH = "https://raw.githubusercontent.com/datasets/covid-19/master/data/time-series-19-covid-combined.csv?opt_id=oeu1600183676142r0.7739378538021684"
      url_NYT = "https://raw.githubusercontent.com/nytimes/covid-19-data/master/us.csv"
      #topic_name = aws_sns_topic.covid_update.arn
      topic_name  = module.admin-sns-email-topic.arn
    }
  }
}

resource "aws_cloudwatch_event_rule" "every_day" {
    name = "CW-Lambda-Covid-Trigger"
    description = "Fires every day"
    schedule_expression = "rate(24 hours)"
}

resource "aws_cloudwatch_event_target" "check_etl_every_day" {
    rule = aws_cloudwatch_event_rule.every_day.name
    target_id = "test_lambda"
    arn = aws_lambda_function.test_lambda.arn
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_etl_lambda" {
    statement_id = "AllowExecutionFromCloudWatch"
    action = "lambda:InvokeFunction"
    function_name = aws_lambda_function.test_lambda.function_name
    principal = "events.amazonaws.com"
    source_arn = aws_cloudwatch_event_rule.every_day.arn
}

module "dynamodb_table" {
  source   = "terraform-aws-modules/dynamodb-table/aws"

  name     = "covid_US"
  hash_key = "Date"
  stream_enabled = true
  stream_view_type = "NEW_IMAGE"
  attributes = [
    {
      name = "Date"
      type = "S"
    }
  ]
}

output "this_dynamodb_table_stream_arn" {
  description = "The ARN of the Table Stream. Only available when var.stream_enabled is true"
  value       = module.dynamodb_table.this_dynamodb_table_stream_arn
}

locals {
    lambda_processor_zip_location = "/tmp/lambda_function_etl_1.zip"
}



data "archive_file" "lambda_function_etl_1" {
  type        = "zip"
  output_path = local.lambda_processor_zip_location
  source_dir = "lambda_function_etl_1"
}
resource "aws_lambda_function" "lambda_processor" {
  filename      = local.lambda_processor_zip_location
  function_name = "firehose_lambda_processor"
  role          = aws_iam_role.lambda_role_for_firehose.arn
  handler       = "lambda_function_stream.lambda_handler"
  memory_size = "192"
  timeout = "20"
  runtime = "python3.7"
  environment {
    variables = {
      DeliveryStreamName = "covid_US"
    }
  }
}

resource "aws_lambda_event_source_mapping" "example" {
  event_source_arn  = module.dynamodb_table.this_dynamodb_table_stream_arn
  function_name     = aws_lambda_function.lambda_processor.arn
  starting_position = "LATEST"
}

resource "aws_kinesis_firehose_delivery_stream" "covid_US" {
  name = "covid_US"
  destination = "s3"

  s3_configuration {
    role_arn = aws_iam_role.role_for_firehose_access_S3.arn
    bucket_arn = aws_s3_bucket.bucket.arn
    buffer_size = 5
    buffer_interval = 120

  }

}

resource "aws_s3_bucket" "bucket" {
  acl    = "private"
}
