# locals {
#     lambda_processor_zip_location = "/tmp/lambda_function_etl_1.zip"
# }

# data "archive_file" "lambda_function_etl_1" {
#   type        = "zip"
#   output_path = local.lambda_processor_zip_location
#   source_dir = "lambda_function_etl_1"
# }


# resource "aws_kinesis_firehose_delivery_stream" "extended_s3_stream" {
#   name        = "terraform-kinesis-firehose-extended-s3-test-stream"
#   destination = "extended_s3"

#   extended_s3_configuration {
#     role_arn   = aws_iam_role.firehose_role.arn
#     bucket_arn = aws_s3_bucket.bucket.arn

#     processing_configuration {
#       enabled = "true"

#       processors {
#         type = "Lambda"

#         parameters {
#           parameter_name  = "LambdaArn"
#           parameter_value = "${aws_lambda_function.lambda_processor.arn}:$LATEST"
#         }
#       }
#     }
#   }
# }

# resource "aws_s3_bucket" "bucket" {
#   acl    = "private"
# }

# resource "aws_iam_role" "firehose_role" {
#   name = "firehose_test_role"

#   assume_role_policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Action": "sts:AssumeRole",
#       "Principal": {
#         "Service": "firehose.amazonaws.com"
#       },
#       "Effect": "Allow",
#       "Sid": ""
#     }
#   ]
# }
# EOF
# }

# resource "aws_iam_role" "lambda_iam" {
#   name = "lambda_iam"

#   assume_role_policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Action": "sts:AssumeRole",
#       "Principal": {
#         "Service": "lambda.amazonaws.com"
#       },
#       "Effect": "Allow",
#       "Sid": ""
#     }
#   ]
# }
# EOF
# }


# resource "aws_lambda_function" "lambda_processor" {
#   filename      = "${local.lambda_zip_location}"
#   function_name = "firehose_lambda_processor"
#   role          = aws_iam_role.lambda_iam.arn
#   handler       = "lambda_function_stream.lambda_handler"
#   memory_size = "192"
#   timeout = "20"
#   runtime = "python3.7"
# }